//
//  Common.h
//  Common
//
//  Created by Polo on 8/16/16.
//  Copyright © 2016 Artygeek. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Common/Log.h>

//! Project version number for Common.
FOUNDATION_EXPORT double CommonVersionNumber;

//! Project version string for Common.
FOUNDATION_EXPORT const unsigned char CommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Common/PublicHeader.h>


